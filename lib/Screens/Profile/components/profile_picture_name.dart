import 'package:flutter/material.dart';
import 'package:one_life/constants.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';

class ProfilePictureName extends StatefulWidget {
  // const ProfilePictureName({
  //   Key key,
  // }) : super(key: key);

  ProfilePictureName(this.imagePickFn, this.isEditable);

  final void Function(File pickedImage) imagePickFn;
  final bool isEditable;

  @override
  _ProfilePictureNameState createState() => _ProfilePictureNameState();
}

class _ProfilePictureNameState extends State<ProfilePictureName> {
  File _pickedImage;

  void _pickImage() async {
    final pickedImageFile = await ImagePicker().getImage(
      source: ImageSource.gallery,
      imageQuality: 50,
      maxWidth: 140,
      maxHeight: 140,
    );
    setState(() {
      _pickedImage = File(pickedImageFile.path);
    });
    widget.imagePickFn(_pickedImage);
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    // This size provides the total height and width of the screen
    return Container(
      // profile picture and name
      color: kPrimaryColor,
      height: size.height * 0.27,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Stack(
            children: <Widget>[
              
              CircleAvatar(
                backgroundColor: Theme.of(context).accentColor,
                backgroundImage:
                    _pickedImage != null ? FileImage(_pickedImage) : null,
                radius: 70,
              ),
              if(widget.isEditable)
              Positioned(
                bottom: 5,
                right: 5,
                child: CircleAvatar(
                  backgroundColor: Colors.white,
                  radius: 15,
                  child: GestureDetector(
                    onTap: _pickImage,
                    child: Icon(Icons.edit),
                  ),
                ),
              )
            ],
          ),
          SizedBox(
            height: size.height * 0.02,
          ),
        ],
      ),
    );
  }
}
