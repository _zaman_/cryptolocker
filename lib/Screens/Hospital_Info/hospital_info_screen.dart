import 'package:flutter/material.dart';
import 'package:one_life/Screens/Hospital_Info/components/hospital_info_card.dart';
import 'package:one_life/Screens/Hospital_Info/components/hospital_map.dart';
import 'package:one_life/Screens/Request_Blood/request_blood_screen.dart';

// root of Hospital INFO Screen
class HospitalInfoScreen extends StatefulWidget {
  HospitalInfoScreen({Key key}) : super(key: key);

  @override
  _HospitalInfoScreenState createState() => _HospitalInfoScreenState();
}

class _HospitalInfoScreenState extends State<HospitalInfoScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              HospitalInfoCard(
                leadingText: "Pusat Pendermaan Darah Hospital Sultanah Aminah",
                subtitleText: "Jalan Mahmoodiah, 80100 Johor Bahru, Johor",
                phoneNumberText: "07-220 0169",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "KPJ Johor Specialist Hospital",
                subtitleText:
                    "39B, Jalan Abdul Samad, Kolam Ayer, 80100 Johor Bahru, Johor",
                phoneNumberText: "07-225 3000",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Pelangi Medical Centre",
                subtitleText:
                    "68, Jalan Kuning, Taman Pelangi, 80400 Johor Bahru, Johor",
                phoneNumberText: "07-333 1263",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "PATHLAB Laboratory Malaysia",
                subtitleText:
                    "Century Garden, 8, Jalan Harimau, Taman Abad, 80250 Johor Bahru, Johor",
                phoneNumberText: "07-333 6063",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Columbia Asia Hospital - Tebrau",
                subtitleText:
                    "5, Kota, Persiaran Southkey 1, Southkey, 80150 Johor Bahru, Johor",
                phoneNumberText: "07-272 9999",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Kempas Medical Centre",
                subtitleText:
                    "Lot PTD 7522, Jalan Kempas Baru, Kempas, 81200 Johor Bahru, Johor",
                phoneNumberText: "07-236 8999",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Hospital Permai Johor Bahru",
                subtitleText:
                    "Persiaran Kempas Baru, Kempas Banjaran, 81200 Johor Bahru, Johor",
                phoneNumberText: "07-231 1000",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Gleneagles Hospital Medini Johor",
                subtitleText:
                    "4, 2, Lebuh Medini Utama, Medini Iskandar, 79250 Nusajaya, Johor",
                phoneNumberText: "07-560 1000",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
              HospitalInfoCard(
                leadingText: "Hospital Pakar Skudai",
                subtitleText:
                    "No. 5 & 7, Jalan Ronggeng 2, Taman Skudai Baru, 81300 Skudai, Johor",
                phoneNumberText: "07-557 0577",
                mobilePress: () {},
                mapText: "Show in Map",
                mapPress: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) {
                        return HospitalMap();
                      },
                    ),
                  );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
