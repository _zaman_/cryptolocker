import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:one_life/Screens/Donate_Blood/donate_blood_screen.dart';
import 'package:one_life/Screens/Hospital_Info/hospital_info_screen.dart';
import 'package:one_life/Screens/Profile/profile_screen.dart';
import 'package:one_life/Screens/Request_Blood/components/request_blood_form.dart';
import 'package:one_life/Screens/Request_Blood/request_blood_screen.dart';

class StartUp extends StatefulWidget {
  StartUp({Key key}) : super(key: key);

  @override
  _StartUpState createState() => _StartUpState();
}

class _StartUpState extends State<StartUp> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        floatingActionButton: FloatingActionButton.extended(
          label: Text('Request Blood'),
          icon: Icon(Icons.add),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) {
                return RequestBloodForm();
              }),
            );
          },
        ),
        appBar: AppBar(
          title: Text("OneLife"),
          actions: [
            IconButton(
              onPressed: () {
                // if pressed, leads to profile screen
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) {
                      return Profile();
                    },
                  ),
                );
              },
              icon: Icon(
                Icons.account_circle_sharp,
                size: 35,
              ),
            ),
          ],
          bottom: TabBar(
            tabs: [
              // Tab(
              //   child: Text(
              //     "REQUEST BLOOD",
              //   ),
              // ),
              Tab(
                child: Text("HOSPITAL INFO"),
              ),
              Tab(
                child: Text("DONATE BLOOD"),
              ),
            ],
          ),
        ),
        body: TabBarView(children: [
          //RequestBloodScreen(),
          HospitalInfoScreen(),
          DonateBloodScreen(),
        ]),
      ),
    );
  }
}
