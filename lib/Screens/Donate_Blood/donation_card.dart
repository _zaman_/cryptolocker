import 'package:flutter/material.dart';
import 'package:one_life/components/rounded_button.dart';

class DonationCard extends StatefulWidget {
  DonationCard(this.name, this.address, this.postCode, this.phone, this.gender,
      this.bloodType, this.quantity,
      {this.key});
  final Key key;
  final String name;
  final String address;
  final String postCode;
  final String phone;
  final String gender;
  final String bloodType;
  final String quantity;
  @override
  _DonationCardState createState() => _DonationCardState();
}

class _DonationCardState extends State<DonationCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      shadowColor: Theme.of(context).primaryColor,
      elevation: 5,
      margin: EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        children: [
          ListTile(
            leading: CircleAvatar(
              backgroundColor: Colors.white,
              foregroundColor: Colors.black,
              child: Text(
                'Z',
                style: TextStyle(fontSize: 20),
              ),
              radius: 30,
            ),
            title: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(widget.name),
                RaisedButton(
                  onPressed: () {},
                  child: Text('Donate'),
                  color: Colors.green[300],
                  elevation: 5,
                ),
              ],
            ),
            subtitle: Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(widget.address + ', ' + widget.postCode),
                    Container(
                      height: 5,
                    ),
                    Row(
                      children: [
                        Text('Blood Type '),
                        CircleAvatar(
                          child: Text(widget.bloodType),
                        ),
                        Text(' Bag Needs '),
                        CircleAvatar(
                          child: Text(widget.quantity),
                        ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
          Container(
            height: 10,
          ),
        ],
      ),
    );
  }
}
