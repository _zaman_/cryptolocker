import 'package:flutter/material.dart';

class HospitalInfoCard extends StatelessWidget {
  final String leadingText;
  final String subtitleText;
  final String phoneNumberText;
  final Function mobilePress;
  final String mapText;
  final Function mapPress;
  const HospitalInfoCard({
    Key key,
    this.leadingText,
    this.subtitleText,
    this.phoneNumberText,
    this.mobilePress,
    this.mapText,
    this.mapPress,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(10),
      elevation: 10,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          ListTile(
            contentPadding: EdgeInsets.all(8),
            leading: Icon(
              Icons.add_location_outlined,
              color: Colors.red,
            ),
            title: Text(
              leadingText,
            ),
            subtitle: Padding(
              padding: const EdgeInsets.only(top: 8, right: 8),
              child: Text(
                subtitleText,
              ),
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              TextButton(
                onPressed: mobilePress,
                child: Text(
                  phoneNumberText,
                ),
              ),
              TextButton(
                onPressed: mapPress,
                child: Text(
                  mapText,
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
