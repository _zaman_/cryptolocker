import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:one_life/Screens/Donor_Eligibility/donor_eligibility.dart';
import 'package:one_life/Screens/Profile/components/profile_picture_name.dart';
import 'package:one_life/Screens/Profile/components/user_information.dart';
import 'package:one_life/components/rounded_button.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:one_life/main.dart';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

// the root of profile screen
class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);
  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  var _showProfile = false;
  // File _userImageFile;

  // void _pickedImage(File image) {
  //   _userImageFile = image;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView(
        children: <Widget>[
          //ProfilePictureName(_pickedImage),
          // SizedBox(
          //   height: 5,
          // ),
          if (_showProfile) UserInformation(),
          Column(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.25,
              ),
              if (!_showProfile)
                RoundedButton(
                  text: "PERSONAL INFORMATION",
                  press: () {
                    setState(() {
                      _showProfile = !_showProfile;
                    });
                  },
                ),
              if (!_showProfile)
                RoundedButton(
                  // customized donor button
                  text: "BECOME A DONOR",
                  press: () {
                    // by pressing takes it takes to the DonorEligibility Screen
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) {
                        return DonorEligibility();
                      }),
                    );
                  },
                ),
              if (!_showProfile)
                RoundedButton(
                  text: "LOG OUT",
                  color: Colors.red,
                  press: () {
                    FirebaseAuth.instance.signOut();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) {
                          return MyApp();
                        },
                      ),
                    );
                  },
                ),
            ],
          ),
        ],
      ),
    );
  }
}
